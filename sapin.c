#include <stdio.h>
#include "others.h"

int	fctrunk(int d)
{
  int	cpt = 0;

  while(cpt < d)
    {
      my_putchar('|');
      cpt++;
    }
  my_putchar('\n');
}


int	dptrunk(int b)
{
  int	dptrunk = 0;
  int	trunk = b;
  int	trunk2 = b;
  int	trunkbloc = b % 2;
  int	trunkspaces = (calc(b) / 2) - 1;
  
  if(trunkbloc == 0)
    {
      trunk++;
      trunkspaces++;
      trunkbloc = trunkbloc + 2;
    }
  if(trunkbloc == 1)
    {
      trunkspaces++;
      trunkbloc++;
    }
  trunkspaces = trunkspaces - (trunk2 / 2);
  while(dptrunk < trunk2)
    {
      fcspaces(trunkspaces);
      fctrunk(trunk);
      dptrunk++;
    }
  trunk--;
}

int	display(int spaces, int stars)
{
  fcspaces(spaces);
  fcstars(stars);
  my_putchar('\n');
  return (stars + 2);
}

void	sapin(int a)
{
  int	dpbloc = 0;
  int	rowbloc = 4;
  int	stars = 1;
  int	spaces = (calc(a) / 2);
  int	bloc = 0;
  int	z = 4;
  int	q = 2;

  while(a-- > 0)
    {
      dpbloc = 0;
      while(dpbloc < rowbloc)
	{
	  stars = display(spaces--, stars);
	  dpbloc++;
	}
      stars = stars - z;
      spaces = spaces + q;
      if(++bloc == 2)
	{
	  z = z + 2;
	  q++;
	  bloc = 0;
	}
      rowbloc++;
    }
}
  
int main(int argc, char **argv)
{
  int i;
  int val;

  if (argc != 2)
    {
      puts("Error.");
      return (0);
    }
  val = atoi(argv[1]);
  if(val <= 0)
    {
      printf("%s\n", "Error. Please enter a positive number !");
      return (0);
    }
  sapin(val);
  dptrunk(val);
  return(0);
}

